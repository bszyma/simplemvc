<?php
/*
 * bootstrap is responsible for autoloading all necessary files
 */

require_once 'config/config.php';

// Load cart
require_once 'classes/Cart.php';

// Load helpers
require_once 'helpers/URLHelpers.php';
require_once 'helpers/SessionHelpers.php';
require_once 'helpers/OtherHelpers.php';

// Autoload all library classes
spl_autoload_register(function($className){
    require_once 'libraries/' . $className . '.php';
});