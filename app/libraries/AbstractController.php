<?php 
/* 
 * Base controller 
 * provides method for loading views and handling models
 */

 abstract class AbstractController
 {
     /*
      * if required model exists return new instance of this model
      */
    public function model($model)
    {
        // load proper model file
        if (file_exists('../app/models/' . $model . '.php')) {
            require_once '../app/models/' . $model . '.php';
        } else {
            die($model . ' could bot be found in ' . 'app/models');
        }

        // instantiate model
        return new $model;
    }

    /*
     * if a view exists render that view
     */
    public function view($view, $data = [])
    {
        if (file_exists('../app/views/' . $view . '.php')) {
            require_once '../app/views/' . $view . '.php';
        } else {
            die($view . ' could not be found in ' . 'app/views/');
        }
    }

 }