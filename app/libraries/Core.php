<?php 
/*
 * App Core Class
 * Creates URL and handles Controllers
 * url = /controller/method/parameters
 */

 class Core 
 {
    protected $currentController = "Home";
    protected $currentMethod = 'index';
    protected $parameters = [];

    public function __construct() 
    {
        $url = $this->getUrl();

        // find and use proper controller
        if (file_exists("../app/controllers/" . ucfirst($url[0]) . ".php")) {
            $this->currentController = ucfirst($url[0]);

            unset($url[0]);
        }
    
        // require controller 
        require_once '../app/controllers/' . $this->currentController . '.php';

        // instantiate controller
        $this->currentController = new $this->currentController;

        // find and use proper method
        if (isset($url[1])) {
            $url[1] = strtolower($url[1]);
            if (method_exists($this->currentController, $url[1])) {
                $this->currentMethod = $url[1];
            }

            unset($url[1]);
        }
        
        // grab parameters
        $this->parameters = $url ? array_values($url) : [];

        // call method with parameter
        call_user_func_array([$this->currentController, $this->currentMethod], $this->parameters);
    }

    public function getUrl()
    {
        if (isset($_GET['url'])) {
            $url = rtrim ($_GET['url'], '/');
            $url = filter_var($url, FILTER_SANITIZE_URL);
            $url = explode('/', $url);

            return $url;
        }
    }

 }
 
