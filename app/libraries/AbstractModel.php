<?php
// abstract classes and interfaces follow the PSR Naming Convention https://www.php-fig.org/bylaws/psr-naming-conventions/
abstract class AbstractModel
{
    /*
     * db handler
     */
    protected $db;

    /*
     * this field might be used if a class needs to override proper method
     * ie. Glass model might want to get all glassES
     * alternatively we might override a getAll() method
     */ 
    protected $getAll = null;

    /*
     * use to define getById for a model if id field in database if is other then id (ie. users_id)
     * or override getById() method
     */
    protected $getById = null;

    /*
     * id for parameters binding
     */
    protected $id = "id";

    /*
     * thanks to the constructor in AbstractModel class I make sure that every model class which extends AbstractModel
     * has access to DataBase handler
     */
    public function __construct()
    {
        $this->db = new Database;
    }

    /*
     * in well structured project which follows some naming conventions (i.e. model singular db table plural)
     * it is a convenient way to "give" some common queries to every model - see examples beneath
     */

    /*
     * get everything from table
     */
    public function getAll() 
    {
        $allQuery = $this->getAll ?? "SELECT * FROM " . get_class($this) . "s";
        $this->db->query($allQuery);
  
        return $this->db->resultSet();
    }

    /*
     * get product of given id frm table
     */
    public function getById($id)
    {
        $getByIdQuery = $this->getById ?? "SELECT * FROM " . get_class($this) . "s WHERE $this->id = :id" ;
        $this->db->query($getByIdQuery);
        $this->db->bind($this->id, $id);

        return $this->db->single();
    }

    /*
     * remove product
     */
    public function delete($id)
    {
        $deleteQuery = $this->delete ?? "DELETE FROM " . get_class($this) . "s WHERE $this->id = :id" ;
        $this->db->query($deleteQuery);
        $this->db->bind($this->id, $id);

        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }
}