<?php
/*
 * this is huge overkill in this framework since basically all libraries are already included in index (via bootstrap)
 * it is merely to implement some dependency injection
 * I ended up never using it
 * I finally decided to leave to show the work flow
 */ 

interface DBInterface
{
    public function __construct();

    public function query($sql);

    public function bind($param, $value, $type = null);

    public function execute();

    public function resultSet();

    public function single();

}