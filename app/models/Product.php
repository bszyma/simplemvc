<?php

class Product extends AbstractModel
{
    /*
     * get price of a product
     */
    public function getPrice($id)
    {
        return $this->getById($id)->price;
    }

    /*
     * get title of a product
     */
    public function getTitle($id)
    {
        return $this->getById($id)->title;
    }

    /*
     * my previous idea how to match elements in the cart with products from db
     * this was not perfect because:
     * 1) where in is slow
     * 2) for very large $range it can break
     * I think that for a cart it is safe to assume that it rarely be bigger than 20 products
     * still I decided to go for looping every element and to get required data via getById and getTitle
     */
    public function getAllWhereIdIn($range)
    {
        if (empty($range)) {
            return [];
        }
        $query = "SELECT * FROM products WHERE id IN ($range)" ;
        $this->db->query($query);

        return $this->db->resultSet();
    }

    /*
     * update existing product
     */
    public function update($data){
        $this->db->query('UPDATE products SET title = :title, price = :price, img = :img WHERE id = :id');
        // Bind values
        $this->db->bind(':id', $data['id']);
        $this->db->bind(':title', $data['title']);
        $this->db->bind(':price', $data['price']);
        $this->db->bind(':img', $data['img']);

        // Execute
        if($this->db->execute()){
            return true;
        } else {
            return false;
        }
    }

    /*
     * insert new product
     */
    public function add($data){
        $this->db->query('INSERT INTO products (title, price, img) VALUES(:title, :price, :img)');
        // Bind values
        $this->db->bind(':title', $data['title']);
        $this->db->bind(':price', $data['price']);
        $this->db->bind(':img', $data['img']);

        // Execute
        if($this->db->execute()){
            return true;
        } else {
            return false;
        }
    }
}