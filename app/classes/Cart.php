<?php


class Cart
{
    private $items = [];
    private $total = 0;

    /*
     * cart is a singleton stored as session variable
     * create new cart if cart does not exist
     * if cart exist return cart
     */
    public static function getCart()
    {
        if (! isset($_SESSION["cart"])) {
            $_SESSION["cart"] = new Cart;
        }

        return $_SESSION["cart"];
    }

    /*
     * get total amount to pay
     */
    public function getTotal()
    {
        return $this->total;
    }

    /*
     * add new item to the cart
     */
    public function addItem($id, $price, $qty)
    {
        $total = $price * $qty;

        if (isset($this->items[$id])) {
            $this->items[$id]["qty"] += $qty;
        } else {
            $this->items[$id]["qty"] = $qty;
        }

        $this->items[$id]["price"] = $price;
        $this->total += $total;
    }

    /*
     * get all items
     */
    public function getItems()
    {
        return $this->items;
    }

    /*
     * check if an item is already in the cart
     */
    public function checkIfInCart($id)
    {
        return (in_array($id, array_keys($this->items)));
    }

    /*
     * remove item from the cart
     */
    public function removeItem($id)
    {
        if (isset($this->items[$id])) {
            $this->total -= $this->items[$id]["qty"] * $this->items[$id]["price"];
            unset($this->items[$id]);
        }
    }
}