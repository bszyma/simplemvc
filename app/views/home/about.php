<?php require_once ROOT . '\views\templates\header.php'?>
<div class="container">
    <h4>Opis zadania. Na cele realizacji zadania postanowiłem napisać własny, prosty framework.</h4>
    <p>W miarę możliwości starałem się podążać za standardami PSR</p>
    <p>Starałem się zachować konwencję nazewniczą "resourceful" (dotyczy) CRUD produktów</p>
    <p>Punktem wejścia w napisanym frameworku jest plik index.php.</p>

    <p>Index.php ładuje plik bootsrap, który z kolei odpowiada za dołączenie wymaganch bibliotek i pliku konfiguracyjnego (zawierającego stałe odpowiadające m.in. za konfigurację bazy danych, nazwę aplikacji itp.).</p>

    <p>Folder app/libraries zawiera bibiloteki frameworka (wszystkie pliki z tego folderu sa ładowane automatycznie przez bootstrap.php):</p>
        <ul>
        <li>core - odpowiada za znalezienie właściwego kontrolera i metody, pełni w zasadzie rolę routera</li>
        <li>abstractcontroller - klasa, z której będą dziedziczyły pozostałe kontrolery - odpowiada za ładowanie modelu i wyświetlanie widoku</li>
        <li>abstractmodel - klasa, z której będą dziedziczyły pozostałe modele - zapewnia uchwyt do bazy, jak również definiuje najczęściej wykorzystywane zapytania do bazy (o ile da się je "generalizować" dla kilku modeli)</li>
        <li>database - klasa zapewniająca połączenie z bazą danych jak również najważniejsze funkcjonalności umożliwiające wykonywanie zapytań do bazy</li>
        <li>folder app/helpers zawiera funkcje pomocnicze podzielone zgodnie z rodzajem oferowanej funkcjonalności (session, url) - wymagane pliki należy manualnie dodać w bootstrap.php</li>
        <li>pliki .htaccess - zapewniają odpowiedni format url</li>
        </ul>

    <h5><strong>Co zrobiłbym inaczej korzystając z "dorosłego" frameworka (np. Laravel): </strong></h5>
        <ol>
            <li>validacja formy odbyłaby się za pomocą FormRequest rules()</li>
            <li>sesja mogłaby być pobierana z DI - można byłoby wykorzystać DI container. Zastanawiałem się na implementacją tego rozwiązania ale ponieważ w swoich prywatnych projektach dopiero przymierzam się do zgłębienia tematu, nie chciałem w projekcie rekrutacyjnym wikłać się w rozwiązanie, w przypadku którego nie miałbym pewności, że zmieszczę się w wyznaczonym czasie. Przy kontenerze miałoby sens oparcie Core.php (klasy będącej w zasadzie routerem) o Reflection API</li>
            <li>DBInterface jest w zasadzie niepotrzebny - w trakcie pisania sądziłem, że będzie sensowne wykorzystanie do DI w konstruktorze modelu ale zarzuciłem ten pomysł ze względu na to, że konstrukcja frameworka przewiduje załączanie wszystkich potrzebnych plików za pomocą bootstrapa</li>
        </ol>
    <p>Powyższe punkty są konsekwencją napisania prostego frameworka, który ogranicza możliwości. Napisanie wymaganych funkcjonalności w Laravelu zajęłoby ok godzinki, może dwóch. Moim sposobem na overkill w tym zadaniu było stworzenie własnego frameworka, na którym zbudowana została wymagana funkcjonalność. W tym miejscu chciałem jedynie zasygnalizować, że widzę pewne słabości wynikające z tego rozwiązania.</p>

</div>
<?php require_once ROOT . '\views\templates\footer.php'?>