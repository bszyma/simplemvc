<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href=" <?php echo URLROOT ?>css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <title><?php echo APPNAME?></title>
</head>
<body>

<div class="container-fluid bg-info text-white text-center">
    <div class="container"></div>
    <h1 class="display-1">Junior Software Engineer assignment</h1>
    <p class="lead">Implementation of cart in custom framework</p>
</div>
<div class="container text-white text-center mb-3">
    <a href="<?php route('about') ?>">
        <button class="btn btn-link btn-lg">About</button>
    </a>
    <a href="<?php route('manageproducts') ?>">
        <button class="btn btn-link btn-lg">Product Manager</button>
    </a>
    <a href="<?php route('products') ?>">
        <button class="btn btn-link btn-lg">Cart</button>
    </a>
</div>

    
