<?php require_once ROOT . '\views\templates\header.php'?>
<div class="container">
    <?php flash('status_message'); ?>
</div>
<div class="container">
    <div class="row">

        <?php foreach ($data['products'] as $product): ?>
            <div class="tie col-sm-12 col-md-6 col-lg-4">
                <form method='POST' action="<?php route('products/add/' . $product->id) ?>">
                    <div class="products">
                        <img src="<?php echo URLROOT . "/public/img/" . $product->img ?>" class="img-fluid img-thumbnail">
                        <h4 class="text-info"><?php echo $product->title; ?></h4>
                        <input type="text" class="form-control mb-3" value="<?php echo $product->price ?>" name="price" readonly>
                        <input type="number" class="form-control mb-3" name="quantity" id="quantity" value="1" min="1" max="1">
                        <input type="submit" value="Add to cart" class="btn btn-info mb-1">
                    </div>
                </form>
            </div>

    <?php endforeach; ?>
    </div>

    <div class="container">
<?php if (!empty($data['inCart'])): ?>
    <div class="table-responsive">
        <table class="table">
            <tr><th colspan="6" ><h3>Order detail</h3></th></tr>
            <tr>
                <th width="5%">No</th>
                <th width="35%">Product title</th>
                <th width="10%">Quantity</th>
                <th width="20%">Price</th>
                <th width="15%">Total</th>
                <th width="5%">Remove</th>
            </tr>
            <?php
                $i = 1;
                foreach ($data['inCart'] as $id => $item):
            ?>

            <tr>
                <td><?php echo $i++ ?></td>
                <td><?php echo $item['title']?></td>
                <td><?php echo $item['qty'] ?></td>
                <td><?php echo $item['price']?></td>
                <td><?php echo $item['total']?></td>
                <td>
                    <a href="<?php route('products/remove/' . $id) ?>">
                        <button class="btn btn-danger">Remove</button>
                    </a>
                </td>
            </tr>
            <?php endforeach ?>
            <tr>
                <td colspan="5" align="right">Total</td>
                <td align="right"><?php echo Cart::getCart()->getTotal() ?></td>
            </tr>

        </table>
    </div>

<?php endif ?>
    </div>
</div>

<?php require_once ROOT . '\views\templates\footer.php'?>