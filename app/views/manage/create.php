<?php require_once ROOT . '\views\templates\header.php'?>

<div class="container">
    <a href="<?php echo URLROOT; ?>manageproducts" class="btn btn-light"><i class="fa fa-backward"></i> Back</a>
    <div class="card card-body bg-light mt-5">
        <h2>Create New Product</h2>
        <form method='POST' action="<?php route('manageproducts/store/') ?>">
            <div class="form-group">
                <label for="title">Title: <sup>*</sup></label>
                <input type="text" name="title" class="form-control form-control-lg <?php echo (!empty($data['title_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo (!empty($data['title'])) ? $data['title'] : "" ?> ">
                <span class="invalid-feedback"><?php echo $data['title_err']; ?></span>
            </div>
            <div class="form-group">
                <label for="price">Price: <sup>*</sup></label>
                <input type="text" name="price" class="form-control form-control-lg <?php echo (!empty($data['price_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo (!empty($data['price'])) ? $data['price'] : "" ?> ">
                <span class="invalid-feedback"><?php echo $data['price_err']; ?></span>
            </div>
            <div class="form-group">
                <label for="img">Image: <sup>*</sup></label>
                <input type="text" name="img" class="form-control form-control-lg" value="<?php echo (!empty($data['img'])) ? $data['img'] : "" ?> ">
            </div>
            <input type="submit" class="btn btn-success" value="Create">
        </form>
    </div>
</div>
<?php require_once ROOT . '\views\templates\footer.php'?>
