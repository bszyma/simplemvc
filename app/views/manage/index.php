<?php require_once ROOT . '\views\templates\header.php'?>
<div class="container">
    <?php flash('product_message'); ?>
    <div class="table-responsive">
        <table class="table">
            <tr>
                <th colspan="3" >
                    <h3>Product detail</h3>
                </th>
                <th colspan="2" >
                <td><form method="POST" action="<?php route('manageproducts/create/') ?>">

                        <input type="submit" class="btn btn-success" value="Add new">
                    </form></td>
                </th>
            </tr>
            <tr>
                <th width="5%">Id</th>
                <th width="35%">Product title</th>
                <th width="10%">Price</th>
                <th width="30%">Image</th>
                <th width="10%">Edit</th>
                <th width="10%">Delete</th>
            </tr>
            <?php

                foreach ($data as $item):
            ?>
            <tr>
                <td><?php echo $item->id ?></td>
                <td><?php echo $item->title?></td>
                <td><?php echo $item->price?></td>
                <td><?php echo $item->img?></td>

                <td>
                    <a href="<?php route('manageproducts/edit/' . $item->id) ?>">
                        <submit class="btn btn-info">Edit</submit>
                    </a>
                </td>
                <td><form method="POST" action="<?php route('manageproducts/delete/' . $item->id) ?>">

                        <input type="submit" class="btn btn-danger" value="Delete">
                    </form></td>
            </tr>
            <?php endforeach ?>
        </table>
    </div>
</div>
<?php require_once ROOT . '\views\templates\footer.php'?>