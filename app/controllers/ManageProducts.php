<?php

class ManageProducts extends AbstractController
{
    /*
     * product model
     */
    private $productModel;

    /*
     * instantiate Product model
     */
    public function __construct()
    {
        $this->productModel = $this->model('Product');
    }

    /*
     * display all existing products
     */
    public function index()
    {
        $products = $this->productModel->getAll();
        return $this->view('manage/index', $products);
    }

    /*
     * load create view
     */
    public function create()
    {
        return $this->view('manage/create');
    }

    /*
     * save new product to db
     */
    public function store()
    {
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Sanitize POST array
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'title' => trim($_POST['title']),
                'price' => $_POST['price'],
                'img' => $_POST['img'],
                'title_err' => '',
                'price_err' => ''
            ];

            $data = $this->validate($data);

            // Make sure no errors
            if (empty($data['title_err']) && empty($data['price_err'])) {
                // Validated
                if ($this->productModel->add($data)) {
                    flash('product_message', 'Product Created');
                    redirect('manageproducts');
                } else {
                    die('Something went wrong');
                }
            } else {
                // Load view with errors
                $this->view('manage/create', $data);
            }
        }
    }

    /*
     * validates input for creating and editing products
     */
    private function validate($data)
    {
        if (empty($data['title'])) {
            $data['title_err'] = 'Please enter title';
        }
        if (!is_numeric($data['price'])) {
            $data['price_err'] = 'Please enter proper number';
        }

        return $data;
    }

    /*
     * load the edit view
     */
    public function edit($id)
    {
        $product = $this->productModel->getById($id);

        $data = [
            'id' => $id,
            'title' => $product->title,
            'price' => $product->price,
            'img' => $product->img,
        ];

        $this->view('manage/edit', $data);
    }

    /*
     * validate input and update product in db
     */
    public function update($id)
    {
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Sanitize POST array
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'id' => $id,
                'title' => trim($_POST['title']),
                'price' => $_POST['price'],
                'img' => $_POST['img'],
                'title_err' => '',
                'price_err' => ''
            ];

            $data = $this->validate($data);

            // Make sure no errors
            if (empty($data['title_err']) && empty($data['price_err'])) {
                // Validated
                if ($this->productModel->update($data)) {
                    flash('product_message', 'Product Updated');
                    redirect('manageproducts');
                } else {
                    die('Something went wrong');
                }
            } else {
                // Load view with errors
                $this->view('manage/edit', $data);
            }
        }
    }

    public function delete($id)
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if($this->productModel->delete($id)) {
                flash('product_message', 'Product deleted');
                redirect('manageproducts');
            } else {
                die('Something went wrong');
            }
        } else {
            redirect('manageproducts');
        }
    }
}