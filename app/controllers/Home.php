<?php 

/*
 * basically responsible for rendering about view
 * index() method is used as a default ending point if no other controller and method is found to handle given url
 */
class Home extends AbstractController
{

    public function index()
    {
        return $this->view('home/about');
    }


    public function about()
    {
        return $this->view('home/about');
    }
}