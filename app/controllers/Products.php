<?php


class Products extends AbstractController
{
    /*
     * product model that will be initialized in constructor
     */
    private $productModel;

    /*
     * instantiate Product model
     */
    public function __construct()
    {
        $this->productModel = $this->model('Product');
    }

    /*
     * display products and show what is in the cart
     * usually cart would be displayed on other view
     * but in this case this is for convenience
     */
    public function index()
    {
        $data['products'] = $this->productModel->getAll();
        $data['inCart'] = $this->inCart();
        return $this->view('products/index', $data);
    }

    /*
     * add new item to the cart
     */
    public function add($id)
    {
        $inCart = Cart::getCart()->checkIfInCart($id);
        $id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        if ($_SERVER["REQUEST_METHOD"] === "POST") {
            $qty = $_POST['quantity'] ?? null;

            //validate input

            if ($inCart || $qty > 1) {
                $msg = "Can't add more than 1 unit";
            } elseif ($qty < 1) {
                $msg = "Can't add less then 1 unit";
            } else {
                Cart::getCart()->addItem($id, $_POST["price"], $_POST["quantity"]);
                $msg = "Item added";
            }
        } else {
            $msg = 'Unknown action';
        }
        flash('status_message', $msg);

        redirect("products");
    }

    /*
     * prepare items in the cart for the view
     */
    private function inCart()
    {
        $products = (Cart::getCart()->getItems());
        /*
         * append title to the item in cart
         */
        foreach ($products as $id => &$product) {
            $product['title'] = $this->productModel->getTitle($id);
            /*
             * this is a matter of taste I do prefer to have all the logic in controllers
             * so that view is only for displaying data for the user
             */
            $product['total'] = $product['qty'] * $product['price'];
        }

        return $products;
    }

    /*
     * remove item from cart
     */
    public function remove($id)
    {
        Cart::getCart()->removeItem($id);
        flash('status_message','Item removed');
        redirect("products");
    }
}