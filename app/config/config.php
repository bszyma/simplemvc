<?php 

// App root
define('ROOT', dirname(dirname(__FILE__)));

//Url root
define('URLROOT', 'http://localhost/simplemvc/');

//App name
define('APPNAME', 'GOG recruitement');

//DB connection
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'test');