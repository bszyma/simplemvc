<?php

// redirect to given url
function redirect($url)
{
    header('location:' . URLROOT . $url);
}

// get route
function route($url)
{
    $url = ($url[strlen($url) - 1] == "/")? $url : $url . "/";
    echo URLROOT . $url;
}