<?php

/*
 * makes debugging easier with formatted var_dump()
 */
function pre_r($var)
{
    echo "<pre>";
    var_dump($var);
    echo "</pre>";
}